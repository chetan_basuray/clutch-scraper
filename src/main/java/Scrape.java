import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Scrape {

    public static void main(String[] args) {
        try{
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the keyword:");
            String keyword = sc.nextLine();
            System.out.println("Enter the location:");
            String location = sc.nextLine();
            PrintWriter pw = new PrintWriter(new File("./files/data-from"+location+"for"+location+".csv"));
            StringBuilder sb = new StringBuilder();

            Scrape scrape = new Scrape();

            int pages = 0;
            //connect to the website
            Connection connection = Jsoup.connect("https://clutch.co/"+keyword+"/"+location+"/");

            //specify user agent
            connection.userAgent("Mozilla/5.0");

            //get the HTML document
            Document doc = connection.get();

            Elements numberOfPages = doc.getElementsByClass("pager-current");
            for(Element page: numberOfPages){
                String text = page.text();
                pages = Integer.parseInt(text.substring(text.indexOf("of")+2).trim());
            }
            System.out.println(pages);


            for(int counter=0; counter < pages; counter++){
            //for(int counter=0; counter < 2; counter++){
                Model model = new Model();
                //connect to the website
                String URL = "https://clutch.co/"+keyword+"/"+location+"?page="+counter;
                System.out.println(URL);
                connection = Jsoup.connect(URL);
                //specify user agent
                connection.userAgent("Mozilla/5.0");
                //get the HTML document
                TimeUnit.SECONDS.sleep(1);
                doc = connection.get();

                Elements contacts = doc.getElementsByClass("item");
                List<String> emails = new ArrayList<String>();
                List<String> phoneNumbers = new ArrayList<String>();
                for(Element contact: contacts){
                    if(contact.toString().contains("icon icon-mail visible-xs-inline")){
                        if(contact.toString().contains("<script>")) {
                            String emailContact = contact.toString();
                            String var = emailContact.substring(emailContact.indexOf("var") + 3, emailContact.indexOf("= '")).trim();
                            emails.add(arrangeEmail(emailContact, var));
                        }else{
                            emails.add("Not Available");
                        }
                    }
                    if(contact.toString().contains("icon-phone")) {
                        String phoneNumber = contact.text();
                        if(phoneNumber.equals(null) || phoneNumber.isEmpty()){
                            phoneNumber = "Not available";
                        }
                        phoneNumbers.add(phoneNumber);
                    }
                }

                model.setEmail(emails);
                model.setPhone(phoneNumbers);

                Elements companyNames = doc.getElementsByClass("company-name");
                List<String> companyNamesList = new ArrayList<String>();
                for(Element companyName: companyNames){
                    companyNamesList.add(companyName.text());
                }
                model.setCompanyName(companyNamesList);

                Elements hourlyRates = doc.getElementsByClass("icon-clock");
                List<String> hourlyRatesList = new ArrayList<String>();
                for(Element rate: hourlyRates){
                    String rateHere = rate.text();
                    if(rateHere.equals(null) || rateHere.isEmpty()){
                        rateHere = "Not available";
                    }
                    hourlyRatesList.add(rateHere);
                }
                model.setRatePerHour(hourlyRatesList);

                Elements addresses = doc.getElementsByClass("location-city");
                List<String> addressList = new ArrayList<String>();
                for(Element address: addresses){
                    addressList.add(address.text());
                }
                model.setAddress(addressList);

                Elements websites = doc.getElementsByClass("website-link");
                List<String> webList = new ArrayList<String>();
                for(Element website: websites){
                    String web = website.toString();
                    String url = web.substring(web.indexOf("href")+5, web.indexOf("rel")).replace("\"","").trim();
                    webList.add(url);
                }
                model.setCompanyUrl(webList);

                for(int i=0; i< model.getCompanyName().size(); i++){
                    System.out.println(model.getCompanyName().get(i));
                }

                scrape.writeToCSV(model, pw, sb);
            }
            pw.close();
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
    }


    private static String arrangeEmail(String emailContact, String var){
        String email = emailContact.substring(emailContact.indexOf("'"), emailContact.indexOf(";")).replace("'","");
        String [] emailArray = email.split("#");
        String algorithm = emailContact.substring(emailContact.indexOf("innerHTML")+11, emailContact.lastIndexOf("document")).replace(";", "").trim().replace(var,"").replace("+", "#");
        String [] algoArray = algorithm.split("#");
        String finalEmail="";
        try {
            for (int i = 0; i < algoArray.length; i++) {
                finalEmail = finalEmail + emailArray[Integer.parseInt(algoArray[i].replace("[", "").replace("]", ""))];
            }
        }catch(Exception e){

        }
        return finalEmail;
    }


    private boolean writeToCSV(Model model, PrintWriter pw, StringBuilder sb){
        try {
            for (int i = 0; i < model.getCompanyName().size(); i++) {
                sb.append(model.getCompanyName().get(i));
                sb.append(',');
                sb.append(model.getCompanyUrl().get(i));
                sb.append(',');
                sb.append(model.getRatePerHour().get(i));
                sb.append(',');
                sb.append(model.getAddress().get(i));
                sb.append(',');
                sb.append(model.getEmail().get(i));
                sb.append(',');
                sb.append(model.getPhone().get(i));
                sb.append('\n');
            }
            pw.write(sb.toString());
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
