import java.util.List;

public class Model {
    private List<String> email;
    private List<String> phone;
    private List<String> companyName;
    private List<String> companyUrl;
    private List<String> ratePerHour;
    private List<String> address;

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    public List<String> getCompanyName() {
        return companyName;
    }

    public void setCompanyName(List<String> companyName) {
        this.companyName = companyName;
    }

    public List<String> getCompanyUrl() {
        return companyUrl;
    }

    public void setCompanyUrl(List<String> companyUrl) {
        this.companyUrl = companyUrl;
    }

    public List<String> getRatePerHour() {
        return ratePerHour;
    }

    public void setRatePerHour(List<String> ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Model{" +
                "email=" + email +
                ", phone=" + phone +
                ", companyName=" + companyName +
                ", companyUrl=" + companyUrl +
                ", ratePerHour=" + ratePerHour +
                ", address=" + address +
                '}';
    }
}